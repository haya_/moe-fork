# SRB2Kart: Moe's Mansion Fork

[SRB2Kart](https://srb2.org/mods/) is a kart racing mod based on the 3D Sonic the Hedgehog fangame [Sonic Robo Blast 2](https://srb2.org/), based on a modified version of [Doom Legacy](http://doomlegacy.sourceforge.net/).

This repo hosts my personal fork of [Moe's Mansion](https://mb.srb2.org/threads/srb2kart-1-3-moe-mansion.29473/), to include some new features.

## Moe's Mansion Features
- Birdhouse included
- Birdmod included
- Large Address Aware so 32-bit builds can use 4gb of memory instead of only 2gb. Helps avoid "out of memory" errors.
- Callmore's new grid character select screen
- Character preview speen by minenice
- Tyron's replaymarker command to mark replays in middle of race

## This fork's features
- James' `local-skins` branch, with some touchups by me.
- X.organic's `futurepk3` branch
- Extended skin and wad limit to 255

**YOU SHOULD NOT USE THIS AS A DEDICATED SERVER. USE VANILLA OR MOE'S MANSION.**

## Dependencies
- NASM (x86 builds only)
- SDL2 (Linux/OS X only)
- SDL2-Mixer (Linux/OS X only)
- libupnp (Linux/OS X only)
- libgme (Linux/OS X only)

## Compiling

See [SRB2 Wiki/Source code compiling](http://wiki.srb2.org/wiki/Source_code_compiling). The compiling process for SRB2Kart is largely identical to SRB2.

## Disclaimer
Kart Krew is in no way affiliated with SEGA or Sonic Team. We do not claim ownership of any of SEGA's intellectual property used in SRB2.
